import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

  const routes = [
    
    {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "Tabla" */ '../views/Home.vue')
  },
  {
    path: '/inventario',
    name: 'Inventario',
    component: () => import(/* webpackChunkName: "Tabla" */ '../views/Inventario.vue')
  },
  {
    path: '/tienda',
    name: 'Tienda',
    component: () => import(/* webpackChunkName: "Carta" */ '../views/Tienda.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
