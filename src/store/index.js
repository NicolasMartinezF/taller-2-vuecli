import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {


    nombre: '',
    descripcion: '',
    precio: 0,
    cantidad: 0,
    producto : [],
    precioTotal1:0,
    precioTotal2:0,



  },
  mutations: {

      },
  getters:{
    comprobar1(){
      return state.nombre === '' ? false : true 
    },
    comprobar2(){
      return state.descripcion === '' ? false : true
    },
    comprobar3(){
      return state.precio <= 0 ? false : true
    },

    calculo(state){
      
      let datos = JSON.parse(localStorage.getItem('carro-vue'));

      if(datos != null){
    
        for(let info of datos){
          var w = info['pre'].replace('.', '')
          var h = parseInt(w.replace('$', ''))
            
        }

        state.precioTotal1 = state.precioTotal1 + h
        console.log(state.precioTotal1)
        return state.precioTotal1
      }
    },

    totali(state){

      let dato1 = JSON.parse(localStorage.getItem('historial-vue'));
      if(dato1 != null){
      
        
      for (let index = 0; index < dato1.length; index++) {
        state.precioTotal2 = state.precioTotal2 + dato1[index]
      }

      return state.precioTotal2

      

    }
    }

  },
  actions: {
  },
  modules: {
  }
})
